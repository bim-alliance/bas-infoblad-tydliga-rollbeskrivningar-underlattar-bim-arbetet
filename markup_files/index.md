![Bild 1](media/1.jpeg)

**Övergripande bild av BIM-strategens och BIM-samordnarens aktiviteter för att säkra vinster med BIM.**

# Tydliga rollbeskrivningar underlättar BIM-arbetet

> ##### Behovet av branschgemensamma befattningsbeskrivningar och BIM-nomenklatur är stort. Därför har en arbetsgrupp utsedd av OpenBIMs projektledargupp arbetat fram ett dokument som underlättar upphandling av BIM-tjänster. Dokumentet konkretiserar roller, ansvar och kompetenskrav och kan fungera som en de facto standard. Det är fritt att använda och kan tankas hem på OpenBIMs hemsida. 

ARBETET MED ATT TA FRAM ROLLBESKRIVNINGAR i BIM-projekt samt att beskriva arbetsuppgifter, kompetens- och erfarenhetskrav för dessa, har pågått från och till sedan slutet av 2012. Arbetsgruppen har bestått av Carl Johan Gårdinger, NCC, Anna Martin, Projektledarhuset, Nina Borgström, White, samt Mats Westerlund, Sweco, som varit sammankallande.
​	– Många är frustrerade över att det är svårt att förstå vad beställarna vill ha och beställarna i sin tur blir frustrerade av att de inte får vad de förväntar sig. Alla parter är intresserade av att rätt roll upphandlas och då behövs tydliga befattningsbeskrivningar. Även i det interna arbetet är det viktigt med klarhet när
det gäller de olika rollerna, säger Mats Westerlund, teknikstrateg VDC på Sweco.
​	I arbetet med att ta fram rollbeskrivningar har synpunkter tagits in från olika håll under ett par remissrundor. Resultatet är alltså väl förankrat bland en mängd olika aktörer. Arbetsgruppen har identifierat två nivåer av roller relaterade till BIM-arbetet, dels en strategisk kallad BIM-strateg, dels en teknisk kallad BIM-samordnare. Gruppen har valt att använda BIM i rollbeteckningen men självklart går det även att använda
VDC. Rollerna är desamma men benämningarna kan variera inom branschen. Till exempel kallas BIM-strategen ibland VDC-manager och samordnaren koordinator. Men det viktiga är rollbeskrivning, ansvar och kompetenskrav, inte vad respektive
roll kallas.
​	En BIM-strateg arbetar med strategiska övergripande frågor tidigt i ett projekt och i nära samarbete med projektledningen. Strategen ansvarar för att ta fram projektets BIMstrategi, ger råd i projektets BIM-frågor och avgör vilken BIMkompetens som behövs. Rollen fanns inte för några år sedan men behovet av en sådan funktion har blivit allt större. Många beslut, som måste tas tidigt i processen, får stor betydelse senare och uppgiften att hantera dessa strategiska frågor blir för tung för projektledaren. Efter att strategen tillsammans med projektets ledning definierat vad som ska göras och det finns en tydlig kravspecifikation inför upphandlingen kan han eller hon ta ett steg tillbaka för att i fortsättningen ha en mer övergripande kontroll av processen.
​	– Kompetensen behövs alltid, inte minst i stora projekt som Slussen och Förbifart Stockholm, men i mindre projekt kan den även i fortsättningen ligga på en annan person, till exempel projekteringsledaren.

EN VIKTIG ROLL FÖR STRATEGEN är att kunna se vad som ger nytta respektive inte nytta till projektet och kunna avgränsa tidigt. Det är sällan man tjänar på att driva allting till det yttersta. I vissa projekt använder man endast redan tidigare beprö-
vade BIM-tillämpningar. I andra fall kan det löna sig att ta stora projektutvecklingskostnader i ett projekt om man därefter har stor nytta av resultaten i ett antal följande projekt.
​	En BIM-strateg bör ha kompetens från liknande projekt och några års erfarenhet av att arbeta med de här frågorna på ett övergripande plan. Det räcker inte att vara tekniskt kunnig i den här rollen som mer handlar om management. Strategen måste veta vad som är möjligt att utföra med tillgänglig teknik och kunna se vilka aktiviteter som kan leda till optimering i projektet.
​	– Det viktiga är att inte stirra sig blind på tekniska saker, de kommer på nästa nivå hos samordnaren, säger Mats Westerlund. Formatdiskussioner och programval ska inte göras för tidigt, eftersom man då lätt blir för snäv och ställer konstiga krav mot projektörerna.

NÄR STRATEGEN FASTSTÄLLT MÅLBILD och ställt krav ska BIM-samordnaren se till att dessa verkställs. Här handlar det till exempel om att på taktik- och metodiknivå få programmen att fungera, att samordna modellerna och leda BIM-samordningsmöten.
Merparten av arbetet äger rum under projektets genomförande. I många projekt finns flera samordnare, både hos beställare, konsulter och entreprenörer. När rollen delas av flera personer är det viktigt att ta fram en tydlig ansvarsfördelning.
​	Dokumentet konkretiserar ansvar och kompetenskrav för BIM-samordnare respektive BIM-strateg i syfte att utifrån projektets behov förenkla inblandning av fler personer än två. Ansvarsfördelningen görs då utifrån aktuella personers erfarenhet
och tänkt roll med dokumentet som stöd. På samma sätt blir det också tydligt att det inte måste vara två skilda personer utan att en person kan inneha båda funktionerna.
​	Samordnarkompetensen behövs så fort man jobbar med en 3D-modell och samordnare finns i princip i alla projekt. En BIM-samordnare ska ha jobbat med frågorna och ha erfarenhet på teknisk nivå, kunna programvarorna, veta vilka som
fungerar med varandra och hur man gör exporter. Det handlar mycket om tekniskt kunnande.

I ARBETSGRUPPENS DOKUMENT FINNS en mängd exempel på kompetensområden som man kan skriva in i sin kravbeställning, en form av inspiration och vägledning till dem som ska upphandla rollerna.
​	Mats Westerlund ser arbetet med rollbeskrivningar som en del av ett större arbete. Dels behöver dokumentet uppdateras efter erfarenhetsåterföring, dels finns fler områden som behöver förtydligas, till exempel vilka styrdokument, exempelvis BIM-manual, som behövs och vad dessa ska inehålla.
​	– Vi ska ta fram en agenda för detta arbete. OpenBIMs uppgift är att se till att arbetet fortsätter.